/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.mail.facade;

import com.company.mail.model.Message;
import com.company.mail.service.MessageRestController;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import org.eclipse.persistence.exceptions.DatabaseException;

/**
 *
 * @author danil
 */
@Stateless
public class MessageFacade extends AbstractFacade<Message> {

    public MessageFacade() {
        super(Message.class);
    }

    public List<Message> getUntreatedMessage(Integer status) {
        return getEntityManager()
                .createQuery("SELECT m FROM Message m where m.status = :status")
                .setParameter("status", status)
                .getResultList();
    }

    // Создание задания на рассылку
    public Message createMessage() {
        Message m = new Message();
        try {
            m.setName("task " + UUID.randomUUID().toString());
            m.setStatus(0);
            Logger.getLogger(MessageFacade.class.getName()).log(Level.INFO, "created {0}", m.getName());
            this.create(m);
            return m;
        } catch (DatabaseException e) {
            Logger.getLogger(MessageFacade.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }

    // Выполнение заданий на рассылку почты
    public List<Message> handleMessage() {
        try {
            List<Message> m = this.getUntreatedMessage(0);
            for (Message ms : m) {
                ms.setStatus(1);
                Logger.getLogger(MessageFacade.class.getName()).log(Level.INFO, "processed {0}", ms.getName());
                this.edit(ms);
                Logger.getLogger(MessageFacade.class.getName()).log(Level.INFO, "sent {0}", ms.getName());
                this.remove(ms);
            }
            return m;
        } catch (DatabaseException e){
            Logger.getLogger(MessageFacade.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }
}
