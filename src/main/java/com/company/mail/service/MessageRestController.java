/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.mail.service;

import com.company.mail.facade.AbstractFacade;
import com.company.mail.facade.MessageFacade;
import com.company.mail.model.Message;
import io.swagger.annotations.Api;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.persistence.exceptions.DatabaseException;

/**
 *
 * @author danil
 */
@Stateless
@Path("message")
@Api(tags = {"MessageRestController"})
public class MessageRestController {
    
    @Inject
    private MessageFacade messageFacade;
    
    // Объект-поставщик, который создает задания на рассылку почты
    @GET
    @Path("createMessage")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Message createMessageToSendMail(){
        try{
            return messageFacade.createMessage();
        }
        catch (Exception e){
            Logger.getLogger(MessageRestController.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }
    
    // Объект-потребитель, который берет задания на рассылку почты
    @GET
    @Path("handleMessage")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public List<Message> handleMessageToSendMail(){
        try{
            return messageFacade.handleMessage();
        } 
        catch (DatabaseException e){
            Logger.getLogger(MessageRestController.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            return null;
        }
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public void create(Message entity) {
        messageFacade.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Message entity) {
        messageFacade.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        messageFacade.remove(messageFacade.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Message find(@PathParam("id") Integer id) {
        return messageFacade.find(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Message> findAll() {
        return messageFacade.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<Message> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return messageFacade.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces({MediaType.APPLICATION_JSON})
    public String countREST() {
        return String.valueOf(messageFacade.count());
    }   
}
